
<%@page import="util.Misc"%>
<%@page import="java.util.HashMap"%>
<%@ page import="entities.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="util.Const"%>
<jsp:include page="<%=Const.PATH_HEAD_JSP%>" />
<jsp:include page="<%=Const.PATH_MENU_JSP%>" />


<%
	User user = (User) session.getAttribute("user");
	if (user == null || user.getPrivilege() != 1) {

		response.sendRedirect("/ISIDrone");
	}
%>


	<div class="container">


		<form method="post" action="newcategorie"
			class="panel panel-primary form-horizontal"
			style="float: unset; margin: auto;">
			<div class="panel-heading">
				<h3 class="panel-title">Ajout d'une nouvelle categorie</h3>
			</div>
			<div class="panel-body">
				<fieldset class="col-sm-6 col-lg-6 col-md-6">
					<legend>Information sur la categorie</legend>


					<div class="form-group">
						<div class="col-sm-10">
							<label for="lastName" class="col-sm-2 control-label">*Nom</label>
							<input type="text" id="name" value="" class="form-control"
								name="name" required />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-10">
							<label for="firstName" class="col-sm-2 control-label">*Description</label>
							<textarea name="description" value="" id="description" rows="5"
								cols="3" class="form-control" required></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10">
							<label for="lastName" class="col-sm-2 control-label">*position</label>
							<input type="number" id="position" class="form-control" value=""
								name="position" required />
						</div>
					</div>
					<div class="col-sm-10">
							<label for="isActive" class="col-sm-8 control-label">*Choisissez
								l'�tat </label> <select class="form-control" name="isActive"
								id="isActive">
								<option class="form-control" value=1>actif</option>
								<option class="form-control" value=0>pas actif</option>
							</select>
						</div>
					</div>


					<button type="submit" class="btn btn-primary">Ajouter</button>
		</form>
	</div>

	<jsp:include page="<%=Const.PATH_FOOTER_JSP%>" />

