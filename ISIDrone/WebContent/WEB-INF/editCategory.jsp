<%@page import="java.util.ArrayList"%>
<%@page import="entities.Category"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="util.Const"%>
<%
	ArrayList<Category> category = (ArrayList<Category>) request.getAttribute("category");
%>
<jsp:include page="<%=Const.PATH_HEAD_JSP%>" />
<jsp:include page="<%=Const.PATH_MENU_JSP%>" />



<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">

				<div class="panel-heading">
					<h1 class="panel-title" style="text-align: center">Modification d'une categorie</h1>
				</div>
				<div style="width:500px; margin-left: auto; margin-right: auto">
					<form action="list-category" method="post">
					
					    <%
							for (Category categorieSelected : category) {
						 %>
						 <div class="form-group">
							<label for="exampleFormControlInput1">Nom</label>
							<input type="text" name="name"  value="<%=categorieSelected.getName()%>"  class="form-control" required autofocus> 
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">Description</label>
							<input type="text" name="description" value="<%=categorieSelected.getDescription()%>"  class="form-control" required> 
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">Position</label>
							<input type="text" name="order" value="<%=categorieSelected.getOrder()%>"  class="form-control" required>
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">Actif</label>
							<input type="text" name="isActive" value="<%=categorieSelected.getIsActive()%>"  class="form-control" required>
						</div>				
						<input type="hidden" name="id" value="<%=categorieSelected.getId()%>"  class="form-control" required>
						<%
						}
						%>	
						<div style="width:150px; margin-left: auto; margin-right: auto">								
							<input type="submit" value="Enregistrer" class="btn btn-primary mb-2" >
						</div>										
					</form>
					<div style="width:110px; margin-left: auto; margin-right: auto">											
							<a href="/ISIDrone/list-category">Annuler</a>
					</div>	
								
				</div>
			</div>
		</div>
	</div>
</div>

	
<jsp:include page="<%=Const.PATH_FOOTER_JSP%>" />