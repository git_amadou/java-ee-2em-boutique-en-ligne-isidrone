<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList"%>
<%@page import="entities.Item"%>
<%@ page import="util.Const"%>
<%@page import="java.text.DecimalFormat"%>

<% 
@SuppressWarnings("unchecked")

ArrayList<Item> items =(ArrayList<Item>) request.getAttribute("items");
%>
<jsp:include page="<%=Const.PATH_HEAD_JSP%>" />
<jsp:include page="<%=Const.PATH_MENU_JSP%>" />


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">

				<div class="panel-heading">
					<h3 class="panel-title">
						<strong>Liste des Produits</strong>
					</h3>
				</div>

				<div class="panel-body">
					<div class="table-responsive">
	
						<div align="center">
						
							<table class="w3-table-all w3-cell w3-cell-middle">
<thead>
								<tr class="w3-amber w3-hover-orange w3-wide">
									<th class="w3-center">Nom</th>
									<th class="w3-centerr">Categorie</th>
									<th class="w3-centerr">Prix  </th>
									<th class="w3-center">Quantite en stock  </th>
									<th class="w3-center">Action  </th>
									<th class="w3-center">Supprimer un produit  </th>
								</tr>
								
								</thead>
																	<%

//Format a deux decimal
DecimalFormat df = new DecimalFormat("####0.00");
if (items.size()>0){
	for(Item item :items){ %>
								<tr class="w3-hover-text-darkgrey ">
				
								
									<td class="w3-center "><strong><%=item.getName()%></strong></td>
									<td class="w3-center"><strong><%=item.getCategory()%></strong></td>

									<td class="w3-right-alig w3-cell-middle"><strong><%=df.format(item.getPrice()) + "$"%></strong></td>

									<td class="w3-center w3-cell-middle"><strong><%=item.getStock() %></strong></td>
									<td class="w3-center w3-cell-middle w3-border w3-hover-amber"> <a href="servletEditProduct?product_id=<%=item.getId()%>">Modifier</a> </td>
									<td class="w3-center w3-cell-middle w3-border w3-hover-amber"><a href="deleteProduct?id-product=<%=item.getId()%>" class="w3-button w3-xlarge w3-circle w3-deep-orange">  supprimer  </a></td>
								</tr>
														<%
		}
	}
	else {
%>
					
						<%
	}
%>
								
							</table>

						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<jsp:include page="<%=Const.PATH_FOOTER_JSP%>" />