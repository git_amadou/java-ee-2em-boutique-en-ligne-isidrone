<%@page import="entities.Order"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="util.Const"%>

<%@ page import="entities.User"%>

<%
	@SuppressWarnings("unchecked")
	ArrayList<Order> allOrders = (ArrayList<Order>) request.getAttribute("allOrders");
%>


<%
	//S'il n'y a pas d'utilisateur d�j� de connect�
	User user = (User) session.getAttribute("user");

	//S'il n'y a pas d'utilisaeur de connect� pr�sentement, on v�rifie dans les cookies
	// redirection vers home
	if (user == null || user.getPrivilege() != 1) {

		response.sendRedirect("/ISIDrone");

	}

	//Si le autoLogin a fonctionn�
%>

<jsp:include page="<%=Const.PATH_HEAD_JSP%>" />
<jsp:include page="<%=Const.PATH_MENU_JSP%>" />

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">

				<div class="panel-heading">
					<h3 class="panel-title">
						<strong>Liste de commandes</strong>
					</h3>
				</div>

				<div class="panel-body">
					<div class="table-responsive">
					
						<table class="table table-condensed">

							<thead>
								<tr>
									<td class="text-center"><strong>Nom du client</strong></td>
									<td class="text-center"><strong>Date de commande</strong></td>
									<td class="text-center"><strong>Actions</strong></td>
									<!--<td class="text-right"><strong>Total</strong></td>-->
								</tr>
							</thead>
							<%
								for (Order unOrder : allOrders) {
							%>
							
							<tr>
								
								<td class="text-center"><label><%=unOrder.getFirstName()%></label></td>
								<td class="text-center"><label><%=unOrder.getNewDate()%></label></td>
								<td class="text-center"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
								Delete</button></td>						
							</tr>
							<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Attention <%=unOrder.getFirstName()%></h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        Voulez-vous vraiment supprimer cette commande ?
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary btn btn-danger" data-dismiss="modal">Annuler</button>
							        <button type="button" class="btn btn-primary btn btn-success"><a href="deleteOrder?id=<%=unOrder.getId()%>">Confirmer</a></button>
							      </div>
							    </div>
	  						</div>
							</div>
							<%
								}
							%>
						</table>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<!-- Footer -->
<jsp:include page="<%=Const.PATH_FOOTER_JSP%>" />