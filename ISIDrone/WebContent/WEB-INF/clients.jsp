<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="util.Const"%>
<%@page import="manager.MUser"%>
<%@page import="entities.User"%>
<%@page import="java.util.ArrayList, entities.Category"%>


<%
	//S'il n'y a pas d'utilisateur déjà de connecté
	User user = (User) session.getAttribute("user");

	//S'il n'y a pas d'utilisaeur de connecté présentement, on vérifie dans les cookies
	// redirection vers home
	if (user == null || user.getPrivilege() != 1) {

		response.sendRedirect("/ISIDrone");

	}

	//Si le autoLogin a fonctionné
%>

<jsp:include page="<%=Const.PATH_HEAD_JSP%>" />
<jsp:include page="<%=Const.PATH_MENU_JSP%>" />

<!-- /.container -->
<!-- Page Content -->
<%
	ArrayList<User> users = (ArrayList<User>) request.getAttribute("clients");
%>



<!-- filtrer avec code js -->
<script src="js/filtre_list_user.js"></script>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">

				<div class="panel-heading">
					<h3 class="panel-title">
						<strong>Liste des clients</strong>
					</h3>
				</div>
				<form class="navbar-form" role="search">
					<div id="auto-search" class="form-group" style="padding-right: 0;">
						<label>filtrer : </label> <input id="filtrer"
							class="form-control biginput" name="search"
							placeholder="Rechercher" autocomplete="off" />
						<div class="form-group login-group-checkbox">

							<input id="name_checkbox" type="checkbox" Checked> <label
								for="name_checkbox">prenom</label>
								
							<input id="prenom_checkbox" type="checkbox" Checked> <label for=prenom_checkbox>nom</label>

							<input id="email_checkbox" type="checkbox" Checked> <label
								for=email_checkbox>email</label>


						</div>

					</div>
					<!--   <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>-->
				</form>

				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-condensed">

							<thead>
								<tr style="background-color: grey;">
									<td class="text-center" style="color: black;"><strong>Prenom
									</strong></td>
									<td class="text-center" style="color: black;"><strong>Nom
									</strong></td>
									<td class="text-center" style="color: black;"><strong>Email</strong></td>
									<td class="text-center" style="color: black;"><strong>Actions</strong></td>
									<!--<td class="text-right"><strong>Total</strong></td>-->
								</tr>
							</thead>



							<%
								for (User unClient : users) {
									if (unClient.getPrivilege() == 0) {
							%>

							<tr class="tr_container">

								<td class="text-center"><label><%=unClient.getLastName()%></label></td>
								<td class="text-center"><label><%=unClient.getFirstName()%></label></td>
								<td class="text-center"><label><%=unClient.getEmail()%></label></td>
								<td class="text-center"><label></label></td>


							</tr>

							<%
								}
								}
							%>

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<jsp:include page="<%=Const.PATH_FOOTER_JSP%>" />