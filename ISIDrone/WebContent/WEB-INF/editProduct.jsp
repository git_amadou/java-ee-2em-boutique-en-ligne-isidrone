<%@page import="entities.User"%>
<%@page import="entities.Item"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="util.Const"%>
<%@page import="action.ActionProducts"%>
<%@page import="manager.MCookies"%>
<%@ page import="util.Const"%>

<%@page import="java.util.ArrayList"%>
<%
Item products = (Item) request.getAttribute("itemProduct");
%>
<%
	//S'il n'y a pas d'utilisateur d�j� de connect�
	User user = (User) session.getAttribute("user");

	//S'il n'y a pas d'utilisaeur de connect� pr�sentement, on v�rifie dans les cookies
	// redirection vers home
	if (user == null || user.getPrivilege() != 1) {
		response.sendRedirect("/ISIDrone");
	}

	//Si le autoLogin a fonctionn�
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script src="js/validation_newProduct.js"></script>
</head>
<body>
	<jsp:include page="<%=Const.PATH_HEAD_JSP%>" />
	<jsp:include page="<%=Const.PATH_MENU_JSP%>" />

	<div class="container">


		<form action="servletEditProduct" method="post"
			class="panel panel-primary form-horizontal"
			style="float: unset; margin: auto;">
			<div class="panel-heading">
			</div>
																	
<%if (products != null){%>
	
			<div class="panel-body">
				<fieldset class="col-sm-6 col-lg-6 col-md-6">
					<legend>Information A modifier</legend>
					 <input type="hidden" id="custId" name="id" value="<%=products.getId()%>">	

					<div class="form-group">
						<div class="col-sm-10">
							<label for="name" class="col-sm-2 control-label">Nom</label> <input
								type="text" class="form-control" name="name" value="<%=products.getName()%>" required />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-10">
							<label for="description" class="col-sm-2 control-label">*Description</label>
							<textarea name="description" rows="5" cols="3"
								class="form-control" value="<%=products.getDescription()%>" required="required"><%= products.getDescription() %></textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-10">
							<label for="price" class="col-sm-2 control-label"
								style="padding-top: 0px;">*Prix</label> <input type="number"
							 class="form-control" name="price" min="1"
								pattern="[0-9]{5,20}" step=".01" value="<%=products.getPrice()%>" required />
						</div>
					</div>


					<div class="form-group">
						<div class="col-sm-10">
							<label for="serialNumber" class="col-sm-8 control-label">*Numero de s�rie</label> <input type="text" class="form-control"
								name="serialNumber"
								 value="<%=products.getSerial()%>" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-10">
							<label for="qtStock" class="col-sm-8 control-label">Quantit�
								en stock</label> <input type="number" class="form-control" id='qtyStock'
								name="qtStock" min=0 pattern="[0-9]{1,100000}" value="<%=products.getStock()%>"  />
						</div>
					</div>
					
						<div class="form-group">
						<div class="col-sm-10">
							<label for="categorie" class="col-sm-8 control-label">Categorie
								en stock</label> <input type="number" class="form-control" id='categorie'
								name="categorie" min=1 pattern="[0-9]{1,10}" value="<%=products.getCategory()%>"  required />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-10">
							<label for="isActive" class="col-sm-8 control-label">
								Etat </label> <input type="number" class="form-control"
								name="isActive" min=0 pattern="[0-9]{1,100000}" "   />
						</div>
					</div>
				</fieldset>

				<div class="form-group text-center"
					style="clear: left; top: 15px; margin-bottom: 15px;">
					<button id='' type="submit"
						class="btn btn-default">Enregistrer Modification</button>
				</div>
			</div>
 <%} %>
		</form>
	</div>


	<jsp:include page="<%=Const.PATH_FOOTER_JSP%>" />