<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="util.Const"%>
<%@page import="util.Misc"%>
<%@page import="java.util.HashMap"%>
<jsp:include page="<%=Const.PATH_HEAD_JSP%>" />
<jsp:include page="<%=Const.PATH_MENU_JSP%>" />

<%
	@SuppressWarnings("unchecked")
	HashMap<String, String> hm_formParamValue = (HashMap<String, String>) request
			.getAttribute("hm_formParamValue");
	@SuppressWarnings("unchecked")
	HashMap<String, String> hm_fieldErrorMsg = (HashMap<String, String>) request
			.getAttribute("hm_fieldErrorMsg");
	String error = (String) request.getAttribute("error");
%>
<!-- Page Content -->
<div class="container">

	<%
		if (error != null) {
			if (error.equals("accountExisting")) {
	%>
	<div class="alert alert-info">Un compte existe d�j� pour cette
		adresse email.</div>
	<%
		} else if (error.equals("DBProblem")) {
	%>
	<div class="alert alert-danger">Une erreur de connexion c'est
		produite. Veuillez attendre quelques temps avant de faire une nouvelle
		tentative. Si vous voyez ce message pour la deuxi�me fois, veuillez
		contactez l'administrateur du site pour lui informer du probl�me.</div>
	<%
		}
		}
	%>

	<form action="signup" method="post"
		class="panel panel-primary form-horizontal"
		style="float: unset; margin: auto;">
		<div class="panel-heading">
			<h3 class="panel-title">Modification du compte</h3>
		</div>
		<div class="panel-body">
			<fieldset class="col-sm-6 col-lg-6 col-md-6">
				<legend>Information Utilisateur</legend>
				<%
					if (hm_fieldErrorMsg != null && hm_fieldErrorMsg.containsKey("lastName")) {
				%>
				<div class="alert alert-warning"
					style="margin-bottom: 0px; white-space: pre-line;"><%=hm_fieldErrorMsg.get("lastName")%></div>
				<%
					}
				%>
				<div class="form-group">
					<div class="col-sm-10">
						<label for="lastName" class="col-sm-2 control-label">*Nom</label>
						<input type="text" id="lastName" class="form-control"
							name="lastName"
							value="")%>"
							required />
					</div>
				</div>
				<%
					if (hm_fieldErrorMsg != null && hm_fieldErrorMsg.containsKey("firstName")) {
				%>
				<div class="alert alert-warning"
					style="margin-bottom: 0px; white-space: pre-line;"><%=hm_fieldErrorMsg.get("firstName")%></div>
				<%
					}
				%>
				<div class="form-group">
					<div class="col-sm-10">
						<label for="firstName" class="col-sm-2 control-label">*Pr�nom</label>
						<input type="text" id="firstName" class="form-control"
							name="firstName"
							value="")%>"
							required />
					</div>
				</div>
				<%
					if (hm_fieldErrorMsg != null && hm_fieldErrorMsg.containsKey("email")) {
				%>
				<div class="alert alert-warning"
					style="margin-bottom: 0px; white-space: pre-line;"><%=hm_fieldErrorMsg.get("email")%></div>
				<%
					}
				%>
				<div class="form-group">
					<div class="col-sm-10">
						<label for="email" class="col-sm-2 control-label">*Email</label> <input
							type="email" id="email" class="form-control" name="email"
							placeholder="Email"
							value="")%>"
							required />
					</div>
				</div>
				<%
					if (hm_fieldErrorMsg != null && hm_fieldErrorMsg.containsKey("confirmEmail")) {
				%>
				<div class="alert alert-warning"
					style="margin-bottom: 0px; white-space: pre-line;"><%=hm_fieldErrorMsg.get("confirmEmail")%></div>
				<%
					}
				%>
				<div class="form-group">
					<div class="col-sm-10">
						<label for="confirmEmail" class="col-sm-2 control-label"
							style="padding-top: 0px;">(Confirmation)<br />*Email
						</label> <input type="email" id="confirmEmail" class="form-control"
							name="confirmEmail" placeholder="Email"
							value="")%>"
							required />
					</div></fieldset>
			<fieldset class="col-sm-6 col-lg-6 col-md-6">
				<legend>Adresse de livraison</legend>
				<%
					if (hm_fieldErrorMsg != null && hm_fieldErrorMsg.containsKey("addr_no")) {
				%>
				<div class="alert alert-warning"
					style="margin-bottom: 0px; white-space: pre-line;"><%=hm_fieldErrorMsg.get("addr_no")%></div>
				<%
					}
				%>
				<div class="form-group">
					<div class="col-sm-10">
						<label for="lastName" class="col-sm-2 control-label">*Num�ro&nbsp;civique</label>
						<input type="text" id="addr_no" class="form-control"
							name="addr_no"
							value="")%>"
							required />
					</div>
				</div>
				<%
					if (hm_fieldErrorMsg != null && hm_fieldErrorMsg.containsKey("addr_appt")) {
				%>
				<div class="alert alert-warning"
					style="margin-bottom: 0px; white-space: pre-line;"><%=hm_fieldErrorMsg.get("addr_appt")%></div>
				<%
					}
				%>
				<div class="form-group">
					<div class="col-sm-10">
						<label for="lastName" class="col-sm-2 control-label">Appartement</label>
						<input type="text" id="addr_appt" class="form-control"
							name="addr_appt"
							value="")%>" />
					</div>
				</div>
				<%
					if (hm_fieldErrorMsg != null && hm_fieldErrorMsg.containsKey("addr_street")) {
				%>
				<div class="alert alert-warning"
					style="margin-bottom: 0px; white-space: pre-line;"><%=hm_fieldErrorMsg.get("addr_street")%></div>
				<%
					}
				%>
				<div class="form-group">
					<div class="col-sm-10">
						<label for="lastName" class="col-sm-2 control-label">*Rue</label>
						<input type="text" id="addr_street" class="form-control"
							name="addr_street"
							value="")%>"
							required />
					</div>
				</div>
				<%
					if (hm_fieldErrorMsg != null && hm_fieldErrorMsg.containsKey("addr_zip")) {
				%>
				<div class="alert alert-warning"
					style="margin-bottom: 0px; white-space: pre-line;"><%=hm_fieldErrorMsg.get("addr_zip")%></div>
				<%
					}
				%>
				<div class="form-group">
					<div class="col-sm-10">
						<label for="lastName" class="col-sm-2 control-label">*Code&nbsp;postal</label>
						<input type="text" id="addr_zip" class="form-control"
							name="addr_zip"
							value="")%>"
							required />
					</div>
				</div>
				<%
					if (hm_fieldErrorMsg != null && hm_fieldErrorMsg.containsKey("addr_city")) {
				%>
				<div class="alert alert-warning"
					style="margin-bottom: 0px; white-space: pre-line;"><%=hm_fieldErrorMsg.get("addr_city")%></div>
				<%
					}
				%>
				<div class="form-group">
					<div class="col-sm-10">
						<label for="lastName" class="col-sm-2 control-label">*Ville</label>
						<input type="text" id="addr_city" class="form-control"
							name="addr_city"
							value="")%>"
							required />
					</div>
				</div>
				<%
					if (hm_fieldErrorMsg != null && hm_fieldErrorMsg.containsKey("addr_state")) {
				%>
				<div class="alert alert-warning"
					style="margin-bottom: 0px; white-space: pre-line;"><%=hm_fieldErrorMsg.get("addr_state")%></div>
				<%
					}
				%>
				<div class="form-group">
					<div class="col-sm-10">
						<label for="lastName" class="col-sm-2 control-label">*Province</label>
						<input type="text" id="addr_state" class="form-control"
							name="addr_state"
							value="")%>"
							required />
					</div>
				</div>
				<%
					if (hm_fieldErrorMsg != null && hm_fieldErrorMsg.containsKey("addr_country")) {
				%>
				<div class="alert alert-warning"
					style="margin-bottom: 0px; white-space: pre-line;"><%=hm_fieldErrorMsg.get("addr_country")%></div>
				<%
					}
				%>
				<div class="form-group">
					<div class="col-sm-10">
						<label for="lastName" class="col-sm-2 control-label">*Pays</label>
						<input type="text" id="addr_country" class="form-control"
							name="addr_country"
							value="")%>"
							required />
					</div>
				</div>
			</fieldset>
			<%
				if (request.getParameter("fromCart") != null) {
			%>
			<input type="hidden" name="fromCart" value="true">
			<%
				}
			%>
			<div class="form-group text-center"
				style="clear: left; top: 15px; margin-bottom: 15px;">
				<button type="submit" class="btn btn-default">Enregistrer les modifications</button>
			</div>
		</div>
	</form>
</div>
<!-- Footer -->
<jsp:include page="<%=Const.PATH_FOOTER_JSP%>" />
