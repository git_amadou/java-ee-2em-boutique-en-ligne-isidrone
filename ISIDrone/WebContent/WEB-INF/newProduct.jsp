<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="util.Const"%>
<%@page import="manager.MLogin"%>
<%@page import="action.ActionLogin"%>
<%@page import="manager.MCookies"%>
<%@ page import="util.Const"%>
<%@ page import="entities.User"%>
<%@ page import="entities.Category"%>
<%@page import="java.util.ArrayList"%>
<%
	ArrayList<Category> categories = (ArrayList<Category>) request.getAttribute("categories");
%>

<%
	String status = "undefined";

	if (request.getAttribute("inserte") != null) {
		boolean functionalInsertion = (boolean) request.getAttribute("inserte");

		if (functionalInsertion) {
			status = "true";

		} else {
			status = "false";

		}

	}
	System.out.println(status);
%>

<%
	//S'il n'y a pas d'utilisateur d�j� de connect�
	User user = (User) session.getAttribute("user");

	//S'il n'y a pas d'utilisaeur de connect� pr�sentement, on v�rifie dans les cookies
	// redirection vers home
	if (user == null || user.getPrivilege() != 1) {

		response.sendRedirect("/ISIDrone");

	}

	//Si le autoLogin a fonctionn�
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script src="js/validation_newProduct.js"></script>
</head>
<body>
	<jsp:include page="<%=Const.PATH_HEAD_JSP%>" />
	<jsp:include page="<%=Const.PATH_MENU_JSP%>" />

	<div class="container">
		<%
			if (status == "true") {
		%>
		<div class="alert alert-info">Nouveau produit ins�r�
			correctement</div>

		<%
			}
		%>

		<%
			if (status == "false") {
		%>
		<div class="alert alert-info">Nouveau produit ins�r�
			correctement</div>


		<div class="alert alert-info">Erreur au moment de l'ex�cution,
			essayez � nouveau</div>

		<%
			}
		%>
		<form action="newProduct" method="post"
			class="panel panel-primary form-horizontal"
			style="float: unset; margin: auto;">
			<div class="panel-heading">
				<h3 class="panel-title">Enregistrement d'un nouveau produit</h3>
			</div>
			<div class="panel-body">
				<fieldset class="col-sm-6 col-lg-6 col-md-6">
					<legend>Information sur le produit</legend>

					<div class="form-group">
						<div class="col-sm-10">
							<label for="name" class="col-sm-2 control-label">*Nom</label> <input
								type="text" id="name" class="form-control" name="name" required />
						</div>
					</div>

					<!--  
					<div class="form-group">
						<div class="col-sm-10">
							<label for="firstName" class="col-sm-2 control-label">*Categorie</label>
							<input type="text" id="firstName" class="form-control"
								name="firstName" required />
						</div>
					</div>
					-->

					<div class="form-group">
						<div class="col-sm-10">
							<label for="firstName" class="col-sm-2 control-label">*Categories
							</label> <select class="form-control" name="category">
								<%
									for (Category category : categories) {
								%>
								<option value="<%=category.getId()%>"><%=category.getName()%></option>

								<%
									}
								%>
							</select>
						</div>
					</div>


					<div class="form-group">
						<div class="col-sm-10">
							<label for="description" class="col-sm-2 control-label">*Description</label>
							<textarea name="description" id="description" rows="5" cols="3"
								class="form-control" required="required"></textarea>
						</div>
					</div>



					<div class="form-group">
						<div class="col-sm-10">
							<label for="price" class="col-sm-2 control-label"
								style="padding-top: 0px;">*Prix</label> <input type="number"
								id="price" class="form-control" name="price" min="1"
								pattern="[0-9]{5,20}" step=".01"
								title="le prix doit contenir seulemet 2 decimales maximal"
								required />
						</div>
					</div>

					<div id='message_serialNumber' class=""
						style="margin-bottom: 0px; white-space: pre-line; display: none;"><label>"pas d'espace, seulement des chiffres et des lettres, minuscules ou
						majuscules"</label></div>

					<div class="form-group">
						<div class="col-sm-10">
							<label for="serialNumber" class="col-sm-8 control-label">*Numero
								de s�rie</label> <input type="text" class="form-control"
								id='serialNumber' name="serialNumber"
								pattern="[A-Za-z0-9]{5,20}"
								title="le numero de serie doit utilice seulment de chiffres et numeros"
								required />
						</div>
					</div>


					<div id='message_qtyStock' class=""
						style="margin-bottom: 0px; white-space: pre-line; display: none;">"pas de lettres, seulement des chiffres"</div>

					<div class="form-group">
						<div class="col-sm-10">
							<label for="qtyStock" class="col-sm-8 control-label">*Quantit�
								en stock</label> <input type="number" class="form-control" id='qtyStock'
								name="qtyStock" min=0 pattern="[0-9]{1,100000}"
								title="seulement de numeros entieres" required />
						</div>
					</div>


					<!--<div class="form-group">
					<div class="col-sm-10">
						  <label  class="col-sm-2 control-label">*cest Active</label> 
							<label for="active" >Actif</label>	<input  type="radio" name="isActive" id="active" value="1" /> 
						<label for="notActive" >Pas actif</label>	<input  type="radio" name="isActive" id="notActive" value="0" />
					</div>
				</div>-->

					<div class="form-group">
						<div class="col-sm-10">
							<label for="isActive" class="col-sm-8 control-label">*Choisissez
								l'�tat </label> <select class="form-control" name="isActive"
								id="isActive">
								<option class="form-control" value=1>actif</option>
								<option class="form-control" value=0>pas actif</option>
							</select>
						</div>
					</div>
				</fieldset>

				<div class="form-group text-center"
					style="clear: left; top: 15px; margin-bottom: 15px;">
					<button id='submit_new_product' type="submit"
						class="btn btn-default">Enregistrer le produit</button>
				</div>
			</div>
		</form>
	</div>



	<jsp:include page="<%=Const.PATH_FOOTER_JSP%>" />
</body>
