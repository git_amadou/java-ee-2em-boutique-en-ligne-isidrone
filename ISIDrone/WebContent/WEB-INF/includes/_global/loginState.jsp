
<%@page import="util.Const"%>
<%@page import="manager.MLogin"%>
<%@page import="action.ActionLogin"%>
<%@page import="manager.MCookies"%>
<%@ page import="entities.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
//S'il n'y a pas d'utilisateur déjà de connecté
User user = (User)session.getAttribute("user");

//S'il n'y a pas d'utilisaeur de connecté présentement, on vérifie dans les cookies
// et si nous somme pas en train de nous déconnecter
if(user == null && request.getAttribute("logout") == null) {
	user = ActionLogin.getUserFromAutoLogin(request);
	session.setAttribute("user", user);
}

//Si le autoLogin a fonctionné
if(user != null) {%>

<li id="loginState">
	<a href="#" id="user"><%=user.getFirstName()%></a>
	<ul id="userAction" class="list-unstyled navbar navbar-default">
	
			<!-- condition pour voir si c'est l'admin qui est connecte ou un client -->
	<%if(user.getPrivilege() == 1){%>
		<!-- code pour ajoute de liens visible seulement avec l'admin -->
		<li><a href="<%="list-products"%>">Liste des produits </a></li>
		<li><a href="<%="list-category"%>">Liste des catégories</a></li>
		<li><a href="<%="newProduct"%>">Ajouter nouveau produit</a></li>
		<li><a href="<%="newcategorie"%>">Ajouter une nouvelle catégorie</a></li>
		<li><a href="<%="listOrders"%>">liste des commandes</a></li>
		<li><a href="<%="clients"%>">liste des clients</a></li>
		<%}else{ %>
		<!-- code si c'est un client qui est connecte -->
		<li><a href="<%="order-history"%>">Historique commande</a></li>
		<%} %>
		<li>&nbsp;</li>
		<li><a href="login">Déconnexion</a></li>
	</ul>
</li>
<% }
else
{
	
%>
<li><a href="signup<%=(request.getParameter("fromCart") != null ? "?fromCart=true" : "")%>">S'enregistrer</a></li>
<li id="loginState"><a href="login">Connexion</a></li>
<% 	
}
%>