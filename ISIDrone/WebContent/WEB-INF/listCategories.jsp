
<%@page import="java.util.ArrayList"%>
<%@page import="entities.Category"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="util.Const"%>

<%
	ArrayList<Category> categories = (ArrayList<Category>) request.getAttribute("categories");

%>

<jsp:include page="<%=Const.PATH_HEAD_JSP%>" />
<jsp:include page="<%=Const.PATH_MENU_JSP%>" />

<%
/*
ArrayList<Category> categoriesIsActive = (ArrayList<Category>) request.getAttribute("categoriesIsActive");
*/

%>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">

				<div class="panel-heading">
					<h3 class="panel-title">
						<strong>Liste des Categories</strong>
					</h3>
				</div>

				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-condensed" id='container-liens'>

							<thead>
								<tr>

									<td class="text-center"><strong>Nom</strong></td>
									<td class="text-center"><strong>Ordre d'affichage</strong></td>
									<td class="text-center"><strong>Actions</strong></td>
								</tr>
							</thead>



							<%
								for (Category category : categories) {
									
							%>

							<tr>
								<td class="text-center"><%=category.getName()%></td>
								<td class="text-center"><%=category.getOrder()%></td>

								<td class="text-center"><button value="delete"
										id="id-<%=category.getId()%>">supprimer</button></td>

								<td class="text-center"><button type="button" class="btn btn-primary mb-2"><a href="edit?id=<%=category.getId()%>">Editer</a></button></td>
								<td class="text-center"><a class="text-center"> <i
										class="fas fa-times"></i>
								</a></td>
</tr>
							<%
								}
							%>

						</table>
						<div id="modal-content">
							<form action="list-category">
								<div class="form-group">
									<p class="text-center ">Voulez-vous supprimer la categorie?</p>
								</div>
								<div class="text-center">
									<button type="submit" name="valider" value="1"
										class="btn btn-success">Oui</button>
									<button type="submit" name="annuler" 
										class="btn btn-danger">Non</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Footer -->
<jsp:include page="<%=Const.PATH_FOOTER_JSP%>" />