const Validation = (function () {
	

    return {
        init: function () {
        	
        	
        	let serialNumber = document.getElementById("serialNumber")
        	let serialNumberError = document.getElementById("message_serialNumber")
        	
        	
        	let qtyStock = document.getElementById("qtyStock")
        	let qtyStockError = document.getElementById("message_qtyStock")
        
        	let btn_submit_new_product = document.getElementById("submit_new_product")
        	
        	
        	serialNumber.addEventListener("keyup",function(){	
        		console.log(serialNumber.value.indexOf("."))
        		if(serialNumber.value.indexOf(" ") != -1 || serialNumber.value.indexOf(".") != -1){
        			
        			console.log('result',serialNumber.value)
        			
        			serialNumberError.setAttribute('class', "alert alert-warning")
        			serialNumberError.style.display = 'block'
        		    btn_submit_new_product.setAttribute('disabled','disabled')	
        		
        		}else{
        			serialNumberError.setAttribute('class', " ")
        			serialNumberError.style.display = 'none'
        			btn_submit_new_product.removeAttribute("disabled")		
        			
        		}
              
            })
            
            qtyStock.addEventListener("keyup",function(){	
            	console.log(qtyStock.value) 
        		if(qtyStock.value.indexOf('.') != -1){        			
        			qtyStockError.setAttribute('class', "alert alert-warning")
        			qtyStockError.style.display = 'block'
        		    btn_submit_new_product.setAttribute('disabled','disabled')	
        		
        		}else{
        			qtyStockError.setAttribute('class', " ")
        			qtyStockError.style.display = 'none'
        			btn_submit_new_product.removeAttribute("disabled")		
        			
        		}
              
            })
        	
        
        }

    }
})()



window.addEventListener('DOMContentLoaded', function loaded(event) {
    window.removeEventListener('DOMContentLoaded', loaded, false)

    // instance d'object Module memoire
    Validation.init()
}, false)
