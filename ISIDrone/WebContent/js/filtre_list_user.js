const Filtrer = (function() {

	return {
		init : function() {

			let filtrer_input = document.getElementById("filtrer")

			let tr_container = document.getElementsByClassName("tr_container")

			let name_checkbox = document.getElementById("name_checkbox")
			let prenom_checkbox = document.getElementById("prenom_checkbox")
			let email_checkbox = document.getElementById("email_checkbox")

			function key_up() {
				let text = filtrer_input.value

				for (let i = 0; i < tr_container.length; i++) {

					// NAME
					if (name_checkbox.checked) {

						if (tr_container[i].children[0].children[0].innerHTML
								.toUpperCase().indexOf(text.toUpperCase()) == -1) {

							console.log('exist ', "false")
							tr_container[i].style.display = 'none'

						} else {

							tr_container[i].style.display = 'table-row'
						}

					}

					// PRENOM
					if (prenom_checkbox.checked) {

						if (tr_container[i].children[1].children[0].innerHTML
								.toUpperCase().indexOf(text.toUpperCase()) == -1) {

							console.log('exist ', "false")
							tr_container[i].style.display = 'none'

						} else {

							tr_container[i].style.display = 'table-row'
						}

					}

					// NAME AND PRENOM

					if (name_checkbox.checked && prenom_checkbox.checked) {

						if (tr_container[i].children[0].children[0].innerHTML
								.toUpperCase().indexOf(text.toUpperCase()) == -1
								&& tr_container[i].children[1].children[0].innerHTML
										.toUpperCase().indexOf(
												text.toUpperCase()) == -1) {

							console.log('exist ', "false")
							tr_container[i].style.display = 'none'

						} else {

							tr_container[i].style.display = 'table-row'
						}

					}

					// EMAIL
					if (email_checkbox.checked) {

						if (tr_container[i].children[2].children[0].innerHTML
								.toUpperCase().indexOf(text.toUpperCase()) == -1) {

							console.log('exist ', "false")
							tr_container[i].style.display = 'none'

						} else {

							tr_container[i].style.display = 'table-row'
						}

					}

					// PRENOM AND EMAIL
					if (prenom_checkbox.checked && email_checkbox.checked) {

						if (tr_container[i].children[1].children[0].innerHTML
								.toUpperCase().indexOf(text.toUpperCase()) == -1
								&& tr_container[i].children[2].children[0].innerHTML
										.toUpperCase().indexOf(
												text.toUpperCase()) == -1) {

							tr_container[i].style.display = 'none'

						} else {

							tr_container[i].style.display = 'table-row'
						}

					}

					// NAME AND EMAIL
					if (name_checkbox.checked && email_checkbox.checked) {

						if (tr_container[i].children[0].children[0].innerHTML
								.toUpperCase().indexOf(text.toUpperCase()) == -1
								&& tr_container[i].children[2].children[0].innerHTML
										.toUpperCase().indexOf(
												text.toUpperCase()) == -1) {

							tr_container[i].style.display = 'none'

						} else {

							tr_container[i].style.display = 'table-row'
						}

					}

					if (!name_checkbox.checked && !prenom_checkbox.checked
							&& !email_checkbox.checked) {

						tr_container[i].style.display = 'table-row'

					}

					// NAME AND PRENOM AND EMAIL
					if (name_checkbox.checked && prenom_checkbox.checked
							&& email_checkbox.checked) {

						if (tr_container[i].children[0].children[0].innerHTML
								.toUpperCase().indexOf(text.toUpperCase()) == -1
								&& tr_container[i].children[1].children[0].innerHTML
										.toUpperCase().indexOf(
												text.toUpperCase()) == -1
								&& tr_container[i].children[2].children[0].innerHTML
										.toUpperCase().indexOf(
												text.toUpperCase()) == -1) {

							console.log('exist ', "false")
							tr_container[i].style.display = 'none'

						} else {

							tr_container[i].style.display = 'table-row'
						}

					}

				}

			}

			filtrer_input.addEventListener("keyup", key_up)

			name_checkbox.addEventListener("change", key_up)

			prenom_checkbox.addEventListener("change", key_up)

			email_checkbox.addEventListener("change", key_up)
		}

	}
})()

window.addEventListener('DOMContentLoaded', function loaded(event) {
	window.removeEventListener('DOMContentLoaded', loaded, false)

	// instance d'object Module memoire
	Filtrer.init()
}, false)
