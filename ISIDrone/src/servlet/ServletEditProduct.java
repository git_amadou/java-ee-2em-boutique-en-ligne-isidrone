package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.ActionCart;
import action.ActionItems;
import action.ActionProducts;
import manager.MItem;
import util.Const;

/**
 * Servlet implementation class ServletEditProduct
 */
@WebServlet("/servletEditProduct")
public class ServletEditProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEditProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int product_id;
	
		try {
			product_id = Integer.parseInt(request.getParameter("product_id"));
		}
		catch(NumberFormatException e) {
			product_id = -1;
		}
		
		ActionProducts.getProductById(product_id, request, response);
		request.getRequestDispatcher(Const.PATH_PAGE_EDITPRODUCT).forward(request, response);
	}	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id;
		
		try {
			id = Integer.parseInt(request.getParameter("id"));
		}
		catch(NumberFormatException e) {
			id = -1;
		}
		String name= request.getParameter("name");
		String description= request.getParameter("description");
		
		int categorie = Integer.parseInt(request.getParameter("categorie"));
		double price = Double.parseDouble(request.getParameter("price"));
		MItem.updateProduct(id, categorie, name, description, price);
		//redirection vers LISTPRODUCT
		ActionItems.getItemsProduct(request, response);
		request.getRequestDispatcher(Const.PATH_PAGE_LISTPRODUCT).forward(request, response);

		//response.sendRedirect(Const.PATH_PAGE_LISTPRODUCT);
		//doGet(request, response);


	
	}

}
