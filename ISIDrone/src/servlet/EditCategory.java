package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.ActionCategory;


@WebServlet(name = "edit", urlPatterns = { "/edit" })
public class EditCategory extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public EditCategory() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Recuperation de la categorie
		
		String id = request.getParameter("id");	
		
		if (id!= null &&! id.isEmpty ()) {
			ActionCategory.getCategoryById(id, request, response);
		}
		ActionCategory.getCategories(request, response);
		
		//Redirection vers la de modification
		request.getRequestDispatcher("/WEB-INF/editCategory.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		
		
		doGet(request, response);
	}

}
