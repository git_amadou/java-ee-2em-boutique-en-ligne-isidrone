package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.ActionClient;
import util.Const;

@WebServlet(name = "clients", urlPatterns = { "/clients" })
public class Client extends HttpServlet {
	private static final long serialVersionUID = 1L;

 
    public Client() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ActionClient.getUsers(request, response);
		request.getRequestDispatcher(Const.PATH_PAGE_CLIENTS).forward(request, response);
	}

}
