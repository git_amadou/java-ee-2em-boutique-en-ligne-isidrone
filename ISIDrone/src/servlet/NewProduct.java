
package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import action.ActionCategory;
import action.ActionItems;
import action.ActionLogin;
import entities.User;
import entities.Item;
import util.Const;

/**
 * Servlet implementation class NewProduct
 */
@WebServlet("/newProduct")
public class NewProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewProduct() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());

		// User user = ActionLogin.getUserFromAutoLogin(request);

		// System.out.println(user == null);

		// User user = (User)session.getAttribute("user");

		// HttpSession session = getSession(request);
		ActionCategory.getCategories(request, response);

		request.getRequestDispatcher(Const.PATH_PAGE_NEWPRODUCT).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);

		String name = request.getParameter("name");
		int category =	Integer.parseInt(request.getParameter("category"));
		String description = request.getParameter("description");
		double price = Double.parseDouble(request.getParameter("price"));
		String serialNumber = request.getParameter("serialNumber");
		int qtyStock = Integer.parseInt(request.getParameter("qtyStock"));
		boolean isActive = Integer.parseInt(request.getParameter("isActive")) == 1?true:false;
		
		//image default pour l'instant
		String img_default = "drone_default.png";
	
	
		
		Item newItem = new Item();
		
		newItem.setName(name);
		newItem.setCategory(category);
		newItem.setDescription(description);
		newItem.setPrice(price);
		newItem.setSerial(serialNumber);
		newItem.setStock(qtyStock);
		newItem.setActive(isActive);
		
		newItem.setImage(img_default);
	
		
		ActionCategory.getCategories(request, response);

		ActionItems.setNewItem(newItem, request, response);
		
		
		request.getRequestDispatcher(Const.PATH_PAGE_NEWPRODUCT).forward(request, response);

	}

}
