package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.ActionCategory;
import entities.Category;
import manager.MCategory;
import util.Const;


@WebServlet("/newcategorie")
public class NewCategory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public NewCategory() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionCategory.getCategories(request, response);

		//request.getRequestDispatcher("/WEB-INF/listCategories.jsp").forward(request, response);
		request.getRequestDispatcher(Const.PATH_PAGE_NEWCATEGORY).forward(request, response);

		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("name");
		String description = request.getParameter("description");
		int position =	Integer.parseInt(request.getParameter("position"));
		int isActive = Integer.parseInt(request.getParameter("isActive"));
		
		Category newCategory = new Category();
		newCategory.setName(name);
		newCategory.setDescription(description);
		newCategory.setOrder(position);
		newCategory.setIsActive(isActive);
		
		

		MCategory.addCategory(newCategory);
		
		System.out.println(name);
		System.out.println(description);
		System.out.println(position);
		System.out.println(isActive);
		System.out.println(newCategory.getId());
		System.out.println(newCategory.getOrder());
		
		
		//doGet(request, response);
		ActionCategory.getCategories(request, response);
		request.getRequestDispatcher(Const.PATH_PAGE_LIST_CATEGORY).forward(request, response);

	}

}
