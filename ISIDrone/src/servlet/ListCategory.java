
package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.ActionCategory;
import manager.MCategory;

import util.Const;


/**
 * Servlet implementation class ListCategory
 */
@WebServlet(
		urlPatterns = { "/list-category" }, 
		initParams = { 
				@WebInitParam(name = "list-category", value = "")
		})
public class ListCategory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListCategory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idTodelete = request.getParameter("valider");
		if(idTodelete!=null) {
			int id=Integer.valueOf(idTodelete);
			MCategory.deleteCategory(id);
		}
		//Récupération des catégories

				ActionCategory.getCategories(request, response);
				ActionCategory.getCategoriesIsActive(request, response);
				//Redirection
			//	request.getRequestDispatcher("/WEB-INF/listCategories.jsp").forward(request, response);

	
		
		//Redirection
		request.getRequestDispatcher(Const.PATH_PAGE_LIST_CATEGORY).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String description = request.getParameter("description");
		String order = request.getParameter("order");
		String isActive = request.getParameter("isActive");
	
		MCategory.editCategory(id, name,description, order, isActive); 
			
		
		 
		 ActionCategory.getCategories(request, response);
		 request.getRequestDispatcher("/WEB-INF/listCategories.jsp").forward(request, response);
		 
		
	}

}
