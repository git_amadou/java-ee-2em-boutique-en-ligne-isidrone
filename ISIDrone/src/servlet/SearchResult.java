package servlet;

import java.awt.peer.SystemTrayPeer;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.ActionCategory;
import action.ActionItems;
import manager.MItem;
import util.Const;

/**
 * Servlet implementation class SearchResult
 */
@WebServlet("/searchResult")
public class SearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	doGet(request, response);
		
		ActionCategory.getCategories(request, response);
		ActionItems.getItems(request, response);
		
		String search = (String)request.getParameter("search");
		
		System.out.println("testing");
		System.out.println(search.isEmpty());
		
		if(search.isEmpty()) {
			search = "888888";
		}
		
		ActionItems.getItemsSearch(search, request, response);
		
		request.setAttribute("placeHolder", search);
		
		request.getRequestDispatcher(Const.PATH_PAGE_ITEMS).forward(request, response);
	}

}
