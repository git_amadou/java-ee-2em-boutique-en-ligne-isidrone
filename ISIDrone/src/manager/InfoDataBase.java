package manager;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
public class InfoDataBase {
	HashMap<String,String>  result = new HashMap();
	InputStream inputStream;
 
	public HashMap getPropValues() throws IOException {
 
		try {
			Properties prop = new Properties();
			//String propFileName = "C:\\isidrone\\conf/configs.properties";
			String propFileName = "configs.properties";
			
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
 
			// save the property value 
			result.put("DRIVER", prop.getProperty("DRIVER"));
			result.put("DB_NAME", prop.getProperty("DB_NAME"));
			result.put("DB_IP", prop.getProperty("DB_IP"));
			result.put("DB_PORT", prop.getProperty("DB_PORT"));
			result.put("DB_USERNAME", prop.getProperty("DB_USERNAME"));
			result.put("DB_PASSWORD", prop.getProperty("DB_PASSWORD"));
			result.put("URL", prop.getProperty("URL"));
 
			
		
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			inputStream.close();
		}
		return result;
	}
}
