
package manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entities.Category;

public class MCategory {

	static String QUERY_GET_CATEGORIES_IS_ACTIVE = "SELECT * FROM category ;";
	static String GET_CATEGORIES = "SELECT * FROM category;";

	public static ArrayList<Category> getCategories() {

		ArrayList<Category> categories = new ArrayList<Category>();

		try {
			MDB.connect();
			String query = GET_CATEGORIES;
			ResultSet rs = MDB.execQuery(query);

			while (rs.next()) {
				categories
						.add(new Category(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5)));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}

		return categories;
	}

	public static void deleteCategory(int id) {
		try {
			MDB.connect();
			String query = "DELETE  FROM category WHERE id=?";
			PreparedStatement ps = MDB.getPS(query);

			ps.setInt(1, id);
			int rs = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}
	}

	public static ArrayList<Category> getCategoryById(String id) {
		ArrayList<Category> category = new ArrayList<Category>();

		try {
			MDB.connect();
			String query = "SELECT * FROM category WHERE id = " + id;
			ResultSet rs = MDB.execQuery(query);
			while (rs.next()) {
				category.add(new Category(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}

		return category;
	}

	public static ArrayList<Category> getCategoriesIsActive() {
		ArrayList<Category> categories = new ArrayList<Category>();
		ArrayList<Category> essai = new ArrayList<Category>();
		try {
			MDB.connect();
			String query = QUERY_GET_CATEGORIES_IS_ACTIVE;
			ResultSet rs = MDB.execQuery(query);
			while (rs.next()) {
				categories
						.add(new Category(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}
		essai = categories;

		return categories;
	}

	public static void editCategory(String id, String name, String description, String order, String isActive) {

		try {
			MDB.connect();
			String query = "UPDATE category SET name ='" + name + "', " + "description='" + description + "',"
					+ "`order`='" + Integer.parseInt(order) + "', " + "isActive='" + Integer.parseInt(isActive) + "' "
					+ "WHERE id =" + Integer.parseInt(id);

			MDB.execQuery(query);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}

	}

	static public int addCategory(Category category) {
		int code = isExist2(category);

		if (code == 1) {
			try {
				MDB.connect();
				// String query = "INSERT INTO category (name, description, order, isActive)
				// VALUES (?, null, ?, ?);";
				String query = "INSERT INTO category (`name`, `description`, `order`, `isActive`) VALUES (?, ?, ?, ?);";

				PreparedStatement ps = MDB.getPS(query);

				ps.setString(1, category.getName());
				ps.setString(2, category.getDescription());
				ps.setInt(3, category.getOrder());
				ps.setInt(4, category.getIsActive());
				ps.executeUpdate(); // MDB.commit();

				/*
				 * ps.setString(1, "test"); ps.setString(2, "Test description"); ps.setInt(3,
				 * 1); ps.setInt(4, 0); ps.executeUpdate();
				 */
				System.out.println(" Successfuly created");
			} catch (SQLException ex) {

			} finally {
				MDB.disconnect();
			}

		}
		return code;
	}

	public static int isExist(int category) {
		int isExist = -1;
		try {
			MDB.connect();
			String query = "SELECT 'exist' FROM category WHERE id = ?";
			PreparedStatement ps = MDB.getPS(query);

			ps.setInt(1, category);
			ResultSet rs = ps.executeQuery();

			isExist = (rs.first() ? 0 : 1);
		} catch (SQLException e) {
			isExist = -1;
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}

		return isExist;
	}

	public static int isExist2(Category category) {
		int isExist = -1;
		try {
			MDB.connect();
			String query = "SELECT id FROM category WHERE name = ?";
			PreparedStatement ps = MDB.getPS(query);

			ps.setString(1, category.getName());
			ResultSet rs = ps.executeQuery();

			isExist = (rs.first() ? 0 : 1);
		} catch (SQLException e) {
			isExist = -1;
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}

		return isExist;
	}

}

