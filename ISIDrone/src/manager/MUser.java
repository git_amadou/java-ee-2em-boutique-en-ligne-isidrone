package manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entities.Category;
import entities.User;

public class MUser {

//static String QUERY_GET_CATEGORIES_IS_ACTIVE = "SELECT * FROM category ;";
	static String QUERY_GET_USERS = "select id,firstName,lastName,email,privilege from user;";

	public static ArrayList<User> getUsers() {

		ArrayList<User> users = new ArrayList<User>();

		try {
			MDB.connect();
			String query = QUERY_GET_USERS;
			ResultSet rs = MDB.execQuery(query);
			while (rs.next()) {
				User un_user = new User();
				un_user.setId(rs.getInt(1));
				un_user.setLastName(rs.getString(2));
				un_user.setFirstName(rs.getString(3));
				un_user.setEmail(rs.getString(4));
				un_user.setPrivilege(rs.getInt(5));

				users.add(un_user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}
		return users;
	}

	public static ArrayList<User> getUserById(String id) {
		ArrayList<User> user = new ArrayList<User>();

		try {
			MDB.connect();
			String query = "SELECT user.id, user.lastName, user.firstName, user.email, user.password,"
					+ "address.id, address.no, address.appt, address.street, address.zip, address.city, address.state, address.country"
					+ " FROM user INNER JOIN address on user.ship_address_id = address.id WHERE user.id = ?";
			ResultSet rs = MDB.execQuery(query);
			while (rs.next()) {
				
				//user.add(new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}

		return user;
	}
}
