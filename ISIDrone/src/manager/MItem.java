
package manager;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import entities.Item;
import entities.SingleEntry;
import entities.User;
import util.Hash;

public class MItem {

	public static ArrayList<Item> getItems(int category) {
		ArrayList<Item> items = new ArrayList<Item>();
		try {
			MDB.connect();
			String query;
			PreparedStatement ps;
			ResultSet rs;

			if (category == 1) {
				query = "SELECT * FROM product";
				ps = MDB.getPS(query);
			} else {
				query = "SELECT * FROM product WHERE category = ?";
				ps = MDB.getPS(query);
				ps.setInt(1, category);
			}

			rs = ps.executeQuery();

			while (rs.next())
				items.add(getItemFromResultSet(rs));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();

		}
		return items;
	}
	public static ArrayList<Item> getItemsProducts() {
		ArrayList<Item> items = new ArrayList<Item>();
		try {
			MDB.connect();
			String query="SELECT * FROM product ";
			ResultSet rs;


			rs = MDB.execQuery(query);

			while (rs.next())
				items.add(getItemFromResultSet(rs));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}
		return items;
	}
	public static void updateProduct(int id, int categorie, String name, String description, double price) {
		String query = "UPDATE product SET category=?,name =?,description=?,price=? WHERE id ="+id;
		try {
			MDB.connect();
			
			PreparedStatement ps=MDB.getPS(query);
			
			ps.setInt(1, categorie);
			ps.setString(3, description);
			ps.setString(2, name);
			ps.setDouble(4, price);
			int rs=ps.executeUpdate();
			int pss=id;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			MDB.disconnect();	
		}
		
	}
	
	public static void deleteProduct(int id) {
		try {
			MDB.connect();
			String query = "DELETE  FROM product WHERE id=?";
			PreparedStatement ps = MDB.getPS(query);

			ps.setInt(1, id);
			int rs = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}
	}


	public static Item getItemById(int id) {
		Item item = null;
		try {
			MDB.connect();
			String query = "SELECT * FROM product WHERE id = ?";

			PreparedStatement ps = MDB.getPS(query);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				item = getItemFromResultSet(rs);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}

		return item;
	}

	public static ArrayList<Item> getFeaturedItems() {
		ArrayList<Item> items = new ArrayList<Item>();
		try {
			MDB.connect();
			String query;
			ResultSet rs;

			query = "SELECT * FROM product WHERE id IN (SELECT product FROM featured_product)";

			rs = MDB.execQuery(query);

			while (rs.next())
				items.add(getItemFromResultSet(rs));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}
		return items;
	}

	public static ArrayList<Item> getItemsSearch(String search) {
		ArrayList<Item> items = new ArrayList<Item>();
		try {
			MDB.connect();
			String query;
			ResultSet rs;

			query = "SELECT * FROM product WHERE upper(name) LIKE upper('%"+search+"%') OR upper(description) LIKE upper('%"+search+"%');";
			
		

			rs = MDB.execQuery(query);

			while (rs.next())
				items.add(getItemFromResultSet(rs));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MDB.disconnect();
		}
		return items;
	}

	private static Item getItemFromResultSet(ResultSet rs) {

		Item item = new Item();

		try {
			item.setId(rs.getInt("id"));
			item.setCategory(rs.getInt("category"));
			item.setName(rs.getString("name"));
			item.setDescription(rs.getString("description"));
			item.setPrice(rs.getDouble("price"));
			item.setSerial(rs.getString("serialNumber"));
			item.setImage(rs.getString("imgName"));
			item.setStock(rs.getInt("stockQty"));
			item.setActive(rs.getBoolean("isActive"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return item;
	}

	public static boolean setNewItem(Item item) {

		boolean linesModified = true;
		
		if (item != null) {
			try {

				MDB.connect();
				// String query = "INSERT INTO autoLogin (`token`, `user`, `date`) VALUES (?, ?,
				// NOW())";
				String query = "INSERT INTO product(id, category, name, description, price, serialNumber,imgName,stockQty,isActive)VALUES (null,?,?,?,?,?,?,?,?);";


				PreparedStatement ps = MDB.getPS(query);
				

				ps.setInt(1, item.getCategory());
				ps.setString(2, item.getName());
				ps.setString(3, item.getDescription());
				ps.setDouble(4, item.getPrice());
				ps.setString(5, item.getSerial());
				ps.setString(6, item.getImage());
				ps.setInt(7, item.getStock());
				ps.setInt(8, (item.isActive() == true ? 1 : 0));

				int resp = ps.executeUpdate();
				
				System.out.println(resp);
				if(resp > 0) {
					linesModified = true;
					
				}else {
					linesModified = false;
				}
				

			} catch ( SQLException e) {
				e.printStackTrace();
			} finally {
				MDB.disconnect();
			}
		} else {
		}

		return linesModified;
	}
}
