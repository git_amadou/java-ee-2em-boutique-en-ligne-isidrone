package action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import manager.MItem;

public class ActionProducts {
	public static void getItemsProduct(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("items", MItem.getFeaturedItems());
	}


	public static void getProductById(int product_id, HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("itemProduct", MItem.getItemById(product_id));
	}

}
