package action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.Item;
import manager.MItem;

public class ActionItems {

	public static void getItems(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("items", MItem.getItems(ActionCategory.getSelectedCategory(request, response)));
	}

	public static void getItemById(int id, HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("item", MItem.getItemById(id));
	}

	public static void getItemsSearch(String search, HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("items", MItem.getItemsSearch(search));
	}

	public static void getItemsProduct(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("items", MItem.getItemsProducts());
	}

	public static void setNewItem(Item unItem,HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("inserte", MItem.setNewItem(unItem));
	}

}

	



