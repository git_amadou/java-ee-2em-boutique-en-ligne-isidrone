package entities;

import java.sql.Date;

public class Order extends Cart {
	private static final long serialVersionUID = 1L;

	int id;
	int userId,isShipped;
	String date, firstName, lastName;
	Date newDate;

	
	public int getIsShipped() {
		return isShipped;
	}

	public void setIsShipped(int isShipped) {
		this.isShipped = isShipped;
	}

	public Date getNewDate() {
		return newDate;
	}

	public void setNewDate(Date newDate) {
		this.newDate = newDate;
	}

	public Order() {
		super();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
